%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Creates isolated 64-bit prefix for SQLyog Community Edition

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Installation
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Download
%%%%%%%%%%%%%%%%%%%%

https://github.com/webyog/sqlyog-community/wiki/Downloads

%%%%%%%%%%%%%%%%%%%%
%%%%% SQLyog
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX='/home/espionage724/Wine Prefixes/SQLyog' wine '/home/espionage724/Downloads/'SQLyog*.exe

%%%%%%%%%%%%%%%%%%%%
%%%%% Finish Up
%%%%%%%%%%%%%%%%%%%%

rm '/home/espionage724/Downloads/'SQLyog*.exe && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Launcher
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Remove Old
%%%%%%%%%%%%%%%%%%%%

rm -R '/home/espionage724/.local/share/applications/wine/Programs/SQLyog Community - 64 bit' && mkdir -p '/home/espionage724/.local/share/applications/wine/Programs/SQLyog'

%%%%%%%%%%%%%%%%%%%%
%%%%% SQLyog
%%%%%%%%%%%%%%%%%%%%

nano '/home/espionage724/.local/share/applications/wine/Programs/SQLyog/SQLyog Community Edition.desktop'

-------------------------
[Desktop Entry]
Name=SQLyog Community Edition
Categories=Development;
Exec=env WINEDEBUG=-all WINEPREFIX='/home/espionage724/Wine Prefixes/SQLyog' wine '/home/espionage724/Wine Prefixes/SQLyog/drive_c/Program Files/SQLyog Community/SQLyogCommunity.exe'
Type=Application
StartupNotify=true
Path=/home/espionage724/Wine Prefixes/SQLyog/drive_c/Program Files/SQLyog Community
Icon=3A9C_SQLyogCommunity.0
-------------------------

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################