%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Creates isolated 32-bit prefix for Diablo II

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Personal Notes
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

WINEPREFIX=~/'Wine Prefixes/Diablo II' winetricks 'vd=800x600'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Installation
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Diablo II
%%%%%%%%%%%%%%%%%%%%

mkdir -p ~/'Wine Prefixes' && WINEPREFIX=~/'Wine Prefixes/Diablo II' WINEARCH=win32 wineboot && WINEPREFIX=~/'Wine Prefixes/Diablo II' winetricks 'grabfullscreen=y' && WINEPREFIX=~/'Wine Prefixes/Diablo II' wine ~/'Downloads/Diablo II/D2-1.12A-enUS/Installer.exe'

%%%%%%%%%%%%%%%%%%%%
%%%%% Lord of Destruction
%%%%%%%%%%%%%%%%%%%%

sync && WINEPREFIX=~/'Wine Prefixes/Diablo II' wine ~/'Downloads/Diablo II/D2LOD-1.12A-enUS/Installer.exe'

%%%%%%%%%%%%%%%%%%%%
%%%%% 1.14b Patch
%%%%%%%%%%%%%%%%%%%%

sync && WINEPREFIX=~/'Wine Prefixes/Diablo II' wine ~/'Downloads/Diablo II/LODPatch_114b.exe'

%%%%%%%%%%%%%%%%%%%%
%%%%% Finish Up
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX=~/'Wine Prefixes/Diablo II' winetricks 'sandbox' && rm -R ~/'Downloads/Diablo II' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Launcher
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial
%%%%%%%%%%%%%%%%%%%%

rm -Rf ~/'.local/share/applications/wine/Programs/Diablo II' && mkdir -p ~/'.local/share/applications/wine/Programs/Diablo II'

%%%%%%%%%%%%%%%%%%%%
%%%%% Lord of Destruction
%%%%%%%%%%%%%%%%%%%%

nano ~/'.local/share/applications/wine/Programs/Diablo II/Diablo II: Lord of Destruction.desktop'

-------------------------
[Desktop Entry]
Name=Diablo II: Lord of Destruction
Categories=Game;
Exec=env WINEDEBUG=-all WINEPREFIX='/home/$USER/Wine Prefixes/Diablo II' wine '/home/$USER/Wine Prefixes/Diablo II/drive_c/Program Files/Diablo II/Diablo II.exe'
Type=Application
StartupNotify=true
Path=/home/$USER/Wine Prefixes/Diablo II/drive_c/Program Files/Diablo II
Icon=CFF8_Diablo II.0
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Quick Commands
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Winecfg
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX=~/'Wine Prefixes/Diablo II' winecfg

%%%%%%%%%%%%%%%%%%%%
%%%%% Winetricks
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX=~/'Wine Prefixes/Diablo II' winetricks

%%%%%%%%%%%%%%%%%%%%
%%%%% Regedit
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX=~/'Wine Prefixes/Diablo II' regedit

%%%%%%%%%%%%%%%%%%%%
%%%%% Kill
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX=~/'Wine Prefixes/Diablo II' wineserver -k

%%%%%%%%%%%%%%%%%%%%
%%%%% File Manager
%%%%%%%%%%%%%%%%%%%%

xdg-open ~/'Wine Prefixes/Diablo II/drive_c/Program Files/Diablo II'

%%%%%%%%%%%%%%%%%%%%
%%%%% Save Backup
%%%%%%%%%%%%%%%%%%%%

cd ~/'Wine Prefixes/Diablo II/drive_c/users/'$USER/'Saved Games' && tar -cvzf ~/'Diablo II-'$(date +%Y-%m-%d)'.tar.gz' 'Diablo II' && cd ~ && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Save Restore
%%%%%%%%%%%%%%%%%%%%

cd ~/'Wine Prefixes/Diablo II/drive_c/users/'$USER/'Saved Games' && tar -xvzf ~/'Diablo II-'*'.tar.gz' 'Diablo II' && cd ~ && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Normal Execute
%%%%%%%%%%%%%%%%%%%%

cd ~/'Wine Prefixes/Diablo II/drive_c/Program Files/Diablo II' && WINEPREFIX=~/'Wine Prefixes/Diablo II' wine ~/'Wine Prefixes/Diablo II/drive_c/Program Files/Diablo II/Diablo II.exe'

%%%%%%%%%%%%%%%%%%%%
%%%%% Performance Graphs
%%%%%%%%%%%%%%%%%%%%

cd ~/'Wine Prefixes/Diablo II/drive_c/Program Files/Diablo II' && vblank_mode=0 GALLIUM_HUD='fps,GPU-load,draw-calls;cpu,cpu0+cpu1+cpu2+cpu3,cpu4+cpu5+cpu6+cpu7;VRAM-usage,requested-VRAM,requested-GTT' WINEPREFIX=~/'Wine Prefixes/Diablo II' wine ~/'Wine Prefixes/Diablo II/drive_c/Program Files/Diablo II/Diablo II.exe'

%%%%%%%%%%%%%%%%%%%%
%%%%% Hammerstorm
%%%%%%%%%%%%%%%%%%%%

xrandr --output 'HDMI-A-0' --off && xrandr --output 'DVI-I-1' --off && cd ~/'Wine Prefixes/Diablo II/drive_c/Program Files/Diablo II' && WINEDEBUG=-all WINEPREFIX=~/'Wine Prefixes/Diablo II' wine ~/'Wine Prefixes/Diablo II/drive_c/Program Files/Diablo II/Diablo II.exe'

- Do gnome-shell --replace (Ctrl + Alt + Backslash) to restore monitors

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################